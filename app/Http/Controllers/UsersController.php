<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

class UsersController extends Controller
{
    /*Export arquivos Excel*/
    public function export()
    {
        return Excel::download(new UsersExport, 'file.xlsx');
    }


    public function import(Request $request)
    {
       return Excel::import(new UsersImport, $request['imported-file']);
    }

}
