<?php

namespace App\Exports;

use App\User;
use Faker\Provider\Color;
use Illuminate\Database\Schema\Blueprint;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

use \Maatwebsite\Excel\Sheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;

class UsersExport implements FromCollection
                            ,WithHeadings
                            ,ShouldAutoSize
                            ,WithEvents
                            ,WithColumnFormatting
                            ,WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {

        // TODO: Implement headings() method.
        return [
            'Id',
            'Name',
            'E-mail',
            'Created_at',
            'Updated_at',
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
//        return [
//
//            AfterSheet::class    => function(AfterSheet $event)
//            {
//                $cellRange = 'A1:E1'; // All headers
//
//                $event->sheet
//                      ->getDelegate()
//                      ->getStyle($cellRange)
//                      ->getFont()
//                      ->setSize(12)
//                      ->setBold(true)
//                      ->setItalic(true);
//            },
//
//        ];

        return [
            AfterSheet::class    => function(AfterSheet $event)
            {
                $event->sheet->styleCells(
                    'A1:E1',
                    [
                        'borders' => array(
                            'outline' => [
                                'borderStyle'   => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                                'color'         => ['argb' => '0000ff'],
                            ],
                        ),

                        'font' => array(
                            'name'      =>  'Calibri',
                            'size'      =>  12,
                            'bold'      =>  true,
                            'color'     => ['argb' => '0000ff'],
                        ),

                    ]
                );

            },
        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        // TODO: Implement map() method.
        return [
            $row->id,
            $row->name,
            $row->email,
            Date::dateTimeToExcel($row->created_at),
            Date::dateTimeToExcel($row->updated_at),
        ];
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        // TODO: Implement columnFormats() method.
        return [
            'D' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'E' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }


}
